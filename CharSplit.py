vowels = ['a', 'A', 'i', 'I', 'u', 'U', 'f', 'F', '{', '}', 'e', 'E', '^', 'o', 'O', '~', '_', '*']

def remove_list_fun(values, j, len_val, check):
    if check == 1:
        values[j + 1] = values[j] + values[j + 1]
        values.remove(values[j])
    else:
        values[j][0] = values[j][0] + " " + values[j + 1][0]
        values.remove(values[j + 1])

    len_val = len_val - 1
    j = j - 1
    return values, j, len_val


def combine_consonents(chars):
    len_chs1 = len(chars)
    j = 0
    while len_chs1 > j + 1:
        if len(chars[j]) == 1:chars, j, len_chs1=remove_list_fun(chars, j, len_chs1,check=1)
        j += 1

    if len(chars[-1]) == 1 and len(chars) > 1:
        chars[-2] = chars[-2] + chars[-1]
        chars.remove(chars[-1])

    for k in range(len(chars)):
        try:
            if chars[k][-1] not in vowels:
                try:
                    chars[k] = chars[k] + chars[k + 1]
                    chars.remove(chars[k + 1])
                except:
                    pass
        except:pass
    return chars

def combine_vowel_consonent(chars):
    len_chs = len(chars)
    i = 0
    while len_chs > i + 1:
        if chars[i] not in vowels and chars[i + 1] in vowels:chars, i, len_chs = remove_list_fun(chars, i, len_chs, check=1)
        i += 1
    return combine_consonents(chars)


def get_char_bigrams(word):
    chars = [ch for ch in word]
    chars = combine_vowel_consonent(chars)
    pos = []
    i = 0
    for ch in chars:
        if i == 0:pos.append([i,i+len(ch)+1])
        else:pos.append([i+1,i+len(ch)+1])
        i = len(ch)

    chars[0] = "_" + chars[0]
    chars[-1] = chars[-1] + "_"
    return chars, pos
