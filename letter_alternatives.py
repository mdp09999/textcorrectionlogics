import ast, pickle
from charSplit import *

## Load trained model
with open('bigramWProb.pkl', 'rb') as handle:
    probabilities_dict = pickle.load(handle)
print("Loaded trained Model!")

codes = {"<s>": 0, "<pad>": 1, "</s>": 2, "<unk>": 3, "|": 4, "!": 5, "#": 6, "$": 7, "*": 8, "-": 9, ":": 10, "?": 11, "@": 12, "A": 13, "B": 14, "C": 15, "D": 16, "E": 17, "F": 18, "G": 19, "H": 20, "I": 21, "J": 22, "K": 23, "L": 24, "M": 25, "N": 26, "O": 27, "P": 28, "Q": 29, "R": 30, "S": 31, "T": 32, "U": 33, "V": 34, "W": 35, "X": 36, "Z": 37, "[": 38, "]": 39, "^": 40, "_": 41, "a": 42, "b": 43, "c": 44, "d": 45, "e": 46, "f": 47, "g": 48, "h": 49, "i": 50, "j": 51, "k": 52, "l": 53, "m": 54, "n": 55, "o": 56, "p": 57, "q": 58, "r": 59, "s": 60, "t": 61, "u": 62, "v": 63, "w": 64, "x": 65, "y": 66, "z": 67, "}": 68, "~": 69}
codes = {v:k for k,v in codes.items()}
vowels = ['a', 'A', 'i', 'I', 'u', 'U', 'f', 'F', '{', '}', 'e', 'E', '^', 'o', 'O', '~', '_', '*']

file = open("/home/mdpauxo/example2.txt","r").read().split("\n")
file = [x for x in file if x != '']

values = []
for line in file:
    if line.split()[:1][0] != "1":
        line = ast.literal_eval(" ".join(line.split()[1:]))
        values.append(line)


len_values = len(values)
i = 0
while len_values > i+1:
    if "|" in values[i] and "|" in values[i+1]:
        values.remove(values[i])
        len_values = len_values-1
        i = i-1
    i += 1
string = str()

j = 0
ulternatives = []
for line in values:
    for k, v in line.items():
        diff = v[0]['prob']-v[1]['prob']
        if diff < 2 and k != '|' and v[1]["letter"] not in [0,1,3] :
            k = "✧"+k
            ulternatives.append([codes[v[0]["letter"]], codes[v[1]["letter"]]])

    if "|" in line:
        try:
            string += "❄"+str(ulternatives)
            ulternatives.clear()
        except:pass


    string += k
    j += 1


def find_balanced_gs(sen):
    stack = []
    for i, c in enumerate(sen):
        if c == "✧":
            if len(stack) == 0:
                yield i

def count_char(test_str, char):
    count = 0
    for i in test_str:
        if i == char:
            count = count + 1
    return count
def replace_str_index(text,index=0,replacement=''):
    return '%s%s%s'%(text[:index],replacement,text[index+1:])

def merge_bigrams(values):
    len_val = len(values)
    i = 0
    while len_val > i+1:
        if values[i][1] == values[i+1][0]:
            values[i] = (values[i][0]+values[i][1],values[i+1][1])
            values.remove(values[i+1])
            i = i-1
            len_val = len_val-1
        i += 1
    return values
def get_before_after(chars,ch2, b):

    if len(chars) == 1:
        return [(chars[b],)], [(ch2,)]
    elif len(chars) == 2 and b == 0:
        return [(chars[b], chars[b+1])],[(ch2, chars[b+1])]
    elif len(chars) == 2 and b == 1:
        return [(chars[b-1], chars[b])],[(chars[b-1], ch2)]
    elif len(chars) == b+1 and len(chars) > 2:
        return [(chars[b-1],chars[b])],[(chars[b-1],ch2)]
    else:return [(chars[b-1],chars[b]), (chars[b],chars[b+1])],[(chars[b-1],ch2), (ch2,chars[b+1])]

string = string.replace("['u', '|']","['u', 'U']")
string= string.split("|")
string = [s for s in string if s != '']


words = []
orig = []
for word in string:
    word, alts = word.split("❄")
    alts = ast.literal_eval(alts)
    positions = [v+1 for v in find_balanced_gs(word)]
    countD =  count_char(word, '✧')
    for i in range(countD):
        positions[i] = positions[i]-i-1

    word = word.replace('✧','')
    orig.append(word)
    if len(alts) > 0:
        print("Main > ", word, alts, positions)
        check_dict = {}
        Chbi, biPo = get_char_bigrams(word)
        final_Bi = {}
        for l, p in zip(alts, positions):
            p = p+1
            b = 0
            for chs, pos in zip(Chbi, biPo):
                if p in range(pos[0],pos[1]):
                    ch1 = chs
                    ch2 = chs.replace(l[0],l[1])
                    c1, c2 = get_before_after(Chbi,ch2, b)
                    cc1 = 0
                    cc2 = 0
                    for lang, counts in probabilities_dict.items():
                        cc1 += sum([counts[c] for  c in c1])
                        cc2 += sum([counts[c] for  c in c2])
                    if cc2 > cc1:
                        final_Bi[b] = ch2
                    else:final_Bi[b] =ch1
                b += 1
        for k, v in final_Bi.items():
            Chbi[k] = v

        corrected_word = "".join(Chbi).replace("_","")
        words.append(corrected_word)
    else:words.append(word)

print(" ".join(words))

print(" ".join(orig))




